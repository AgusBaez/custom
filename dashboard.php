<?php

include('database.php');

session_start();
if(!isset($_SESSION['rol'])){
        header('location:login.php');
    }
    else{
        if($_SESSION['rol'] != 2){
            header('location:login.php');
        }
    }

$mysqli = Conectar();

$consulta = "select * from producto";

$items = mysqli_query($mysqli,$consulta);

;
?>
<?php include('includes/header.php');?>
<div class="container p-4">
    <div class="row">
        <!-- Recuadro agregar Prod -->
        <div class="col-md-4">
        <h5>Agregrar Producto</h5>
            <div class="card card-body">
                <form action="save_prod.php" method="POST">
                    <div class="form-group">
                        <h5>Titulo</h5>
                        <input type="text" name="titulo" class="form-control"
                        placeholder="Agrega un Titulo" autofocus>
                    </div>
                    <div class="from-group">
                        <h5>Descripcion</h5>
                        <textarea name="descripcion" rows="2" class="from-control" 
                        placeholder="Agrega la Descripcion"></textarea>
                    </div>
                    <div class="form-group">
                        <h5>Precio</h5>
                        <input type="number" name="precio" min="0" value="0" step=".01" class="form-control"placeholder="Precio" autofocus>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="save_prod" value="Cargar">
                </form>
            </div>
            <!--mensaje de producto cargado/ELIMINADO-->
            <?php if (isset($_SESSION['mensaje'])) { ?>
                <div class="alert alert-<?= $_SESSION['mensaje_type']; ?> alert-dismissible fade show"  role="alert">
                <?= $_SESSION['mensaje'] ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <?php session_unset(); } ?>
        </div>
        <!-- Lado derecho de la pagina -->
        <div class="col-md-8">
                <table class="table table-bordered">
                <thead class="table table-dark table-striped">
            <tr>
                <td>ID PROD</td>
                <td>titulo</td>
                <td>descripcion</td>
                <td>precio</td>
                <td>fecha</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach($items as $item)
        { ?>
            <tr>
            <td><?php echo $item['idProd']; ?></td>
            <td><?php echo $item['titulo']; ?></td>
            <td><?php echo $item['descripcion']; ?></td>
            <td>$ <?php echo $item['precio']; ?></td>
            <td><?php echo $item['fecha']; ?></td>
            <td><a href="delete.php?id=<?php echo $item['idProd']; ?>"class="btn btn-danger" >
            <i class="far fa-trash-alt">
            </i>
            </a> </td>
            </tr>
        <?php 
            } //cierro el foreach
        ?>
        </tbody>
    </table>
                </table>
            </div>
        </div>
    </div>
<?php include("includes/footer.php") ?>